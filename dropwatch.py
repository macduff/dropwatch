import pyinotify
import sys
import re
import ConfigParser
from StringIO import StringIO
import os.path
import pwd
import grp
import os

import functools

class Counter(object):
    """
Simple counter.
"""
    def __init__(self):
        self.count = 0
    def plusone(self):
        self.count += 1

def on_loop(notifier, counter):
    """
Dummy function called after each event loop, this method only
ensures the child process eventually exits (after 5 iterations).
"""
    if False:
        # Loops 5 times then exits.
        sys.stdout.write("Exit\n")
        notifier.stop()
        sys.exit(0)
    else:
        sys.stdout.write("Loop %d\n" % counter.count)
        counter.plusone()

# Notifier example from tutorial
#
# See: http://github.com/seb-m/pyinotify/wiki/Tutorial
#
wm = pyinotify.WatchManager()  # Watch Manager
# watched events
mask = pyinotify.IN_DELETE | \
       pyinotify.IN_CREATE | \
       pyinotify.IN_ACCESS | \
       pyinotify.IN_MODIFY | \
       pyinotify.IN_ATTRIB | \
       pyinotify.IN_CLOSE_WRITE | \
       pyinotify.IN_MOVED_FROM  | \
       pyinotify.IN_MOVED_TO | \
       pyinotify.IN_ISDIR

def eventHelper(event):
    print "Directory: " + str(event.dir)
    print "Mask: " + str(event.mask)
    print "Maskname: " + str(event.maskname)
    print "Name: " + str(event.name)
    print "Path: " + str(event.path)
    print "Pathname: " + str(event.pathname)
    print "WD: " + str(event.wd)
    f = str(event.pathname)
    force_group = 'bbusers'
    stat_info = os.lstat(f)
    for kk in ACCESSDB.keys():
      print '    Look in ' + kk
      if( f.startswith( ACCESSDB[kk]['path'] ) ):
        print '      Found it!'
        force_group = ACCESSDB[kk]['group']
      # endif
    # endfor
    print 'Group must be ' + force_group
    share_gid = grp.getgrnam(force_group).gr_gid
    if( stat_info.st_gid != share_gid ):
      # change the group id, need to change the owner
      # so just make it bbuser for now
      print "GROUP IS WRONG FIX!"
      uid = pwd.getpwnam("bbuser").pw_uid
      os.chown(path, uid, gid)
    #endif

    #uid = pwd.getpwnam("nobody").pw_uid
    #gid = grp.getgrnam("nogroup").gr_gid
    #path = '/tmp/f.txt'
    #os.chown(path, uid, gid)

class EventHandler(pyinotify.ProcessEvent):
    def process_IN_CREATE(self, event):
        print "Creating:", event.pathname
        eventHelper(event)

    def process_IN_DELETE(self, event):
        print "Removing:", event.pathname

    def process_IN_ACCESS(self, event):
        print "Accessing:", event.pathname
        eventHelper(event)

    def process_IN_MODIFY(self, event):
        print "Modifying:", event.pathname
        eventHelper(event)

    def process_IN_ATTRIB(self, event):
        print "Modify attribute:", event.pathname

    def process_IN_CLOSE_WRITE(self, event):
        print "Close write:", event.pathname

    def process_IN_MOVE_FROM(self, event):
        print "Move from:", event.pathname

    def process_IN_MOVE_TO(self, event):
        print "Modifying:", event.pathname
        
    def process_IN_ISDIR(self, event):
        print "Event against dir:", event.pathname
# endclass

def noGlobal(x):
  return 'global' != x

def noGroup(x):
  return x.find('@') == -1

DEBUG = True
WROOT = '/srv/samba'
ACCESSDB = dict()

def parseSmbConfig(fname):
  data = StringIO('\n'.join(line.strip() for line in open(fname)))
  parser = ConfigParser.RawConfigParser() #SafeConfigParser()
  parser.readfp(data)
  sections = filter(noGlobal,parser.sections())
  shareNames = list()
  shareConfig = dict()
  access = dict()

  # get the share names
  for section in sections:
    for name, value in parser.items( section ):
      if( name == 'path' and \
          value.find('printers') == -1 and \
          value.find('spool') == -1 ):
        shareNames.append(section)
      #endif
    #endif
  #endif
  
  # collect all the share configuration into a single
  # dictionary
  for shareName in shareNames:
    sDict = dict()
    for name, value in parser.items( shareName ):
      sDict[name] = value
    #endif
    shareConfig[shareName] = sDict
  #endif

  # now, try to find the users allowed users, or group
  for shareName in shareNames:
    access[shareName] = dict()
    sDict = shareConfig[shareName]
    if( sDict.has_key('valid users') ):
      access[shareName]['owners'] = sDict['valid users']
    #endif
    if( sDict.has_key('write list') ):
      if( access[shareName].has_key('owners') ):
        access[shareName]['owners'] = access[shareName]['owners'] + " " + sDict['write list']
      else:
        access[shareName]['owners'] = sDict['write list']
      #endif
    #endif
    if( not access[shareName].has_key('owners') ):
      access[shareName]['owners'] = 'bbuser'
    #endif
    access[shareName]['group'] = sDict['force group']
    owners = re.split('[, ]+',access[shareName]['owners'])
    userOwners = filter(noGroup,owners)
    if( len(userOwners) == 0 ):
      # no individual owner identified, use defualt owner
      access[shareName]['owner'] = 'bbuser'
    else:
      # just use the first owner as the default
      access[shareName]['owner'] = userOwners[0]
    #endif
    access[shareName]['path'] = sDict['path']
    if( DEBUG == True ):
      print access[shareName]['path'] + ' -> ' + access[shareName]['owner'] + \
        ':' + access[shareName]['group']
    #endif
  #endfor
  return access
#endfunction

if __name__ == '__main__':
  if( len(sys.argv) < 2 ):
    print("Enter configuration file name!")
    sys.exit(-1)
  else:
    if( os.path.isfile(sys.argv[1]) ):
      fname = sys.argv[1]
    else:
      print("Argument is not a file!")
      sys.exit(-1)
    #endif
  #endif
  if( len(sys.argv) > 2 ):
    parseTest = 1
  else:
    parseTest = 0
  #endif
  access = parseSmbConfig(fname)
  ACCESSDB = access
  if( DEBUG == True ):
    for share in access:
      pass
      #access[share]['path'] = '/home/cwynkoop/tmp' + access[share]['path']
    # endfor
  # endif
  if( parseTest == 0 ):
    # TODO
    # check for which share the changed file resides
    # check for whether the new file has obeyed the "force group" rule
    # if it did not obey the force group rule, change the owner and
    # group
    on_loop_func = functools.partial(on_loop, counter=Counter())

    handler = EventHandler()
    notifier = pyinotify.Notifier(wm, handler)
    wdd = wm.add_watch(WROOT, mask, rec=True)
    # Notifier instance spawns a new process when daemonize is set to True. This
    # child process' PID is written to /tmp/pyinotify.pid (it also automatically
    # deletes it when it exits normally). Note that this tmp location is just for
    # the sake of the example to avoid requiring administrative rights in order
    # to run this example. But by default if no explicit pid_file parameter is
    # provided it will default to its more traditional location under /var/run/.
    # Note that in both cases the caller must ensure the pid file doesn't exist
    # before this method is called otherwise it will raise an exception.
    # /tmp/pyinotify.log is used as log file to dump received events. Likewise
    # in your real code choose a more appropriate location for instance under
    # /var/log (this file may contain sensitive data). Finally, callback is the
    # above function and will be called after each event loop.
    try:
      notifier.loop(daemonize=True, callback=on_loop_func,
                      pid_file='/tmp/pyinotify.pid', stdout='/tmp/pyinotify.log')
    except pyinotify.NotifierError, err:
      print >> sys.stderr, err
      notifier.loop()
  #endif
#endif

