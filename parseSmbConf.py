import sys
import ConfigParser
from StringIO import StringIO
import os.path

if __name__ == '__main__':
  if( len(sys.argv) < 2 ):
    print("Enter configuration file name!")
    sys.exit(-1)
  else:
    if( os.path.isfile(sys.argv[1]) ):
      fname = sys.argv[1]
    else:
      print("Argument is not a file!")
      sys.exit(-1)
    #endif
  #endif
  data = StringIO('\n'.join(line.strip() for line in open(fname)))
  parser = ConfigParser.RawConfigParser() #SafeConfigParser()
  parser.readfp(data)
  for section in parser.sections():
    print section
    for name, value in parser.items( section ):
      print '  %s = %r' % ( name, value )
#endif
